package spring.hrd.advance.springhrdadvancesource.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "mn_notification")
public class Notification {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String targetGroupName;
    private String type;
    private Timestamp logDate;
    private Timestamp modifiedDate;
    private Boolean isRead;
    private Boolean status;
    private String notificationUrl;

    @ManyToOne
    @JoinColumn(name = "target_user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "target_group_id")
    private Group group;

    @ManyToOne
    @JoinColumn(name = "post_id")
    private Post post;


    @OneToMany(
            mappedBy = "notification",
            cascade = CascadeType.ALL
    )
    private List<ReadNotification> readNotifications;
}
