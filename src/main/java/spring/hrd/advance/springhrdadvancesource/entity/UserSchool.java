package spring.hrd.advance.springhrdadvancesource.entity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "mn_user_school")
public class UserSchool{

    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "first",column = @Column(name = "user_id")),
            @AttributeOverride(name = "second",column = @Column(name = "school_id"))
    })
    private PrimaryId primaryId;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("first")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("second")
    private School school;

    private Timestamp startedYear;
    private Timestamp graduatedYear;
    private Timestamp createdDate;
    private Timestamp modifiedDate;
    private Boolean status;
}
