package spring.hrd.advance.springhrdadvancesource.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "mn_user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String firstName;
    private String lastName;
    private String username; // old version is userName
    @Column(length = 1)
    private String gender;
    private Timestamp dob;
    private String email;
    private String facebookId;
    private Timestamp createDate;
    private Timestamp modifiedDate;
    private String description;
    @Column(length = 1,columnDefinition = "varchar(1) default 'U'")
    private String role;
    private String profileUrl;
    private String coverUrl;
    private Boolean status;
    private Boolean isActive;

    @OneToMany(
            mappedBy = "user",
            orphanRemoval = true,
            cascade = CascadeType.ALL
    )
    private List<UserSchool> userSchools;

    @ManyToOne(
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            optional = false
    )
    @JoinColumn(name = "location_id")
    private Location locations;

    @OneToMany(
            mappedBy = "user",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL
    )
    private List<MateList> mateLists;


    @OneToMany(
            mappedBy = "user",
            fetch = FetchType.LAZY,
            orphanRemoval = true,
            cascade = CascadeType.ALL
    )
    private List<UserGroup> userGroups;

    @OneToMany(
            mappedBy = "user",
            fetch = FetchType.LAZY,
            orphanRemoval = true
    )
    private List<Post> posts;

    @OneToMany(
            mappedBy = "user",
            fetch = FetchType.LAZY,
            orphanRemoval = true
    )
    private List<Comment> comments;

    @OneToMany(
            mappedBy = "user",
            fetch = FetchType.LAZY,
            orphanRemoval = true
    )
    private List<Notification> notifications;

    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL
    )
    private List<ReadNotification> readNotifications;

}
