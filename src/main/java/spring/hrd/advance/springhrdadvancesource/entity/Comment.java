package spring.hrd.advance.springhrdadvancesource.entity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "mn_comment")
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL
    )
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL
    )
    @JoinColumn(name = "post_id")
    private Post post;

    private String commentText;
    private Timestamp commentDate;
    private Timestamp modifiedDate;
    private String postUrl;
    private Boolean status;
}
