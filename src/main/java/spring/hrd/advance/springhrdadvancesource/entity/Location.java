package spring.hrd.advance.springhrdadvancesource.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "mn_location")
public class Location {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String provinceName;
    private String districtName;
    private Boolean status;

    @OneToMany(
            fetch = FetchType.LAZY,
            mappedBy = "locations",
            cascade = CascadeType.ALL
    )
    private List<User> users;

    @OneToMany(
            mappedBy = "locations",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL
    )
    private List<School> schools;
}
