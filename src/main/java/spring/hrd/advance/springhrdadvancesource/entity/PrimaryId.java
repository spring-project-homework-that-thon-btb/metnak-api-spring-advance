package spring.hrd.advance.springhrdadvancesource.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class PrimaryId implements Serializable {

    @Column
    private Integer first;

    @Column
    private Integer second;
}
