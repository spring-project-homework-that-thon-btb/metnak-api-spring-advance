package spring.hrd.advance.springhrdadvancesource.entity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "mn_mate_list")
public class MateList {
    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "first",column = @Column(name = "user_id")),
            @AttributeOverride(name = "second",column = @Column(name = "mate_id"))
    })
    private PrimaryId primaryId;

    private Timestamp relationshipDate;
    private Boolean status;

    @ManyToOne(
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            optional = false
    )
    @MapsId("first")
    private User user;

}
