package spring.hrd.advance.springhrdadvancesource.entity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "mn_read_notification")
public class ReadNotification {

    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "first",column = @Column(name = "user_id")),
            @AttributeOverride(name = "second",column = @Column(name = "notification_id"))
    })
    private PrimaryId primaryId;

    private Timestamp readDate;

    @ManyToOne(
            fetch = FetchType.LAZY
    )
    @MapsId("first")
    private User user;


    @ManyToOne(
            fetch = FetchType.LAZY
    )
    @MapsId("first")
    private Notification notification;

}
