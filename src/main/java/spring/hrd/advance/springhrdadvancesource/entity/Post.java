package spring.hrd.advance.springhrdadvancesource.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "mn_post")
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String content;
    private String photoUrl;
    private Timestamp createdDate;
    private Timestamp modifiedDate;
    private Boolean status;

    @ManyToOne(
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            optional = false
    )
    @JoinColumn(name = "group_id")
    private Group group;

    @ManyToOne(
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            optional = false
    )
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(
            mappedBy = "post",
            fetch = FetchType.LAZY
    )
    private List<Like> likes;

    @OneToMany(
            mappedBy = "post",
            fetch = FetchType.LAZY,
            orphanRemoval = true
    )
    private List<Comment> comment;

    @OneToMany(
            mappedBy = "post",
            fetch = FetchType.LAZY,
            orphanRemoval = true
    )
    private List<Notification> notifications;

}
