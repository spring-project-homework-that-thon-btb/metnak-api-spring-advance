package spring.hrd.advance.springhrdadvancesource.entity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "mn_like")
public class Like {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Timestamp likeDate;
    private Timestamp modifiedDate;

    @Column(columnDefinition = "Boolean default true")
    private Boolean status;

    @ManyToOne(
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL
    )
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL
    )
    @JoinColumn(name = "post_id")
    private Post post;
}
