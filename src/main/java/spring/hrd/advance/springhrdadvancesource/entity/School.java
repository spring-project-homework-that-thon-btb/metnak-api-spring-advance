package spring.hrd.advance.springhrdadvancesource.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "mn_school")
public class School {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String level;
    private Boolean status;

    @OneToMany(
            mappedBy = "school",
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            orphanRemoval = true
    )
    private List<UserSchool> userSchools;

    @ManyToOne(
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            optional = false
    )
    @JoinColumn(name = "location_id")
    private Location locations;

}
