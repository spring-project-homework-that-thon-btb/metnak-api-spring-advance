package spring.hrd.advance.springhrdadvancesource.entity;


import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "mn_group")
public class Group {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;

    @Column(columnDefinition = "timestamp default NOW()")
    private Timestamp createdDate;
    private Timestamp modifiedDate;
    private Boolean status;

    @OneToMany(
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            mappedBy = "group"
    )
    private List<UserGroup> userGroups;

    @OneToMany(
            fetch = FetchType.LAZY,
            mappedBy = "group",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Post> posts;

    @OneToMany(
            mappedBy = "group"
    )
    private List<Notification> notifications;
}
