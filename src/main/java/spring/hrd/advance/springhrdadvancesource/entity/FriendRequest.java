package spring.hrd.advance.springhrdadvancesource.entity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "mn_friend_request")
public class FriendRequest {
    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "first",column = @Column(name = "sender_id")),
            @AttributeOverride(name = "second",column = @Column(name = "receiver_id"))
    })
    private PrimaryId primaryId;

    private Timestamp receivedDate;

    @Column(columnDefinition = "boolean default false")
    private Boolean status;

    @Column(columnDefinition = "boolean default false")
    private Boolean confirm;

    @ManyToOne(
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL
    )
    @MapsId("first")
    private User user;
}
