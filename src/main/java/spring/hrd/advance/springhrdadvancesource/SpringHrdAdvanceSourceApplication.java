package spring.hrd.advance.springhrdadvancesource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringHrdAdvanceSourceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringHrdAdvanceSourceApplication.class, args);
    }

}

